export const environment = {
  PUSHER_APP_KEY: 'app-key',
  PUSHER_APP_CLUSTER: 'mt1',
  PUSHER_HOST: '192.168.1.103',
  PUSHER_PORT: 6001,
  API_BASE_URL: 'http://localhost:8000/api/'
};
