import { Injectable } from '@angular/core';
import { Observable, take } from "rxjs";
import { ApiService } from "../../core/services/api.service";

export interface Mission {
  id: number;
  name: string;
  joined: boolean;
  created_at: Date;
  updated_at: Date;
}

@Injectable({
  providedIn: 'root'
})
export class MissionsService {
  constructor(
    private api: ApiService
  ) {
  }

  getMissions$(): Observable<Mission[]> {
    return this.api.get$<Mission[]>('missions');
  }

  joinMission$(missionId: number): Observable<any> {
    return this.api.post$(`missions/${missionId}/join`, {})
      .pipe(take(1));
  }

  leaveMission$(missionId: number): Observable<any> {
    return this.api.post$(`missions/${missionId}/leave`, {})
      .pipe(take(1));
  }
}
