import { Component, inject } from '@angular/core';
import { MissionListComponent } from "../../components/mission-list/mission-list.component";
import { MissionsService } from "../../services/missions.service";
import { AuthService } from "../../../core/services/auth.service";
import { BehaviorSubject, switchMap, take } from "rxjs";
import { Router } from "@angular/router";

@Component({
  selector: 'app-missions',
  template: `
    <div class="header">
      <h1>Missions</h1>
      <div>
        <span>{{ auth.user?.name }}</span>
        <button type="button" (click)="logout()">Log out</button>
      </div>
    </div>
    <hr>
    <div class="mission-list">
      <app-mission-list [missions$]="missions$"
                        (success)="missionRefresh$.next(null)"
      ></app-mission-list>
    </div>
  `,
  styleUrls: ['./missions.page.scss'],
  standalone: true,
  imports: [
    MissionListComponent,
  ]
})
export class MissionsPage {
  protected missionRefresh$ = new BehaviorSubject(null);
  protected missions$ = this.missionRefresh$
    .pipe(switchMap(() => this.missions.getMissions$()));
  protected missions = inject(MissionsService);
  protected auth = inject(AuthService);
  private router = inject(Router);

  logout() {
    this.auth.logout$()
      .pipe(take(1))
      .subscribe(() =>
        this.router.navigate(['/login']).then());
  }
}
