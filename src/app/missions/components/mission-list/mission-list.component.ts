import { Component, EventEmitter, Input, Output } from '@angular/core';
import { MissionListItemComponent } from "../mission-list-item/mission-list-item.component";
import { AsyncPipe, NgForOf } from "@angular/common";
import { Observable } from "rxjs";
import { Mission } from "../../services/missions.service";
import { RouterLink } from "@angular/router";

@Component({
  selector: 'app-mission-list',
  template: `
    <table>
      <thead>
      <tr>
        <th>ID</th>
        <th>Name</th>
        <th>Joined</th>
        <th>Created At</th>
        <th>Actions</th>
      </tr>
      </thead>
      <tbody>
      <tr app-mission-list-item
          *ngFor="let mission of missions$ | async"
          [routerLink]="['/missions', mission.id, 'chat']"
          [mission]="mission"
          (success)="success.emit()"
      ></tr>
      </tbody>
    </table>
  `,
  styleUrls: ['./mission-list.component.scss'],
  standalone: true,
  imports: [
    AsyncPipe,
    NgForOf,
    MissionListItemComponent,
    RouterLink
  ]
})
export class MissionListComponent {

  @Input({ required: true }) missions$!: Observable<Mission[]>;

  @Output() success: EventEmitter<void> = new EventEmitter<void>();

  constructor() {
  }
}
