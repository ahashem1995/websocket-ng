import { Component, EventEmitter, inject, Input, Output } from '@angular/core';
import { Mission, MissionsService } from "../../services/missions.service";
import { DatePipe, NgIf } from "@angular/common";

@Component({
  selector: '[app-mission-list-item]',
  template: `
    <td>{{ mission.id }}</td>
    <td>{{ mission.name }}</td>
    <td *ngIf="mission.joined">&#10003;</td>
    <td *ngIf="!mission.joined">&#215;</td>
    <td>{{ mission.created_at | date }}</td>
    <td>
      <button *ngIf="!mission.joined" type="button" class="join-btn"
      (click)="$event.stopPropagation(); joinMission()">Join</button>
      <button *ngIf="mission.joined" type="button" class="leave-btn"
      (click)="$event.stopPropagation(); leaveMission()">Leave</button>
    </td>
  `,
  styleUrls: ['./mission-list-item.component.scss'],
  standalone: true,
  imports: [
    DatePipe,
    NgIf
  ]
})
export class MissionListItemComponent {
  @Input({ required: true }) mission!: Mission;

  @Output() success: EventEmitter<void> = new EventEmitter<void>();

  private missions = inject(MissionsService);

  joinMission() {
    this.missions.joinMission$(this.mission.id)
      .subscribe(() => {
        this.success.emit();
      });
  }

  leaveMission() {
    this.missions.leaveMission$(this.mission.id)
      .subscribe(() => {
        this.success.emit();
      });
  }
}
