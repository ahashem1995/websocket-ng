import { Injectable } from '@angular/core';
import { ApiService } from "../../core/services/api.service";
import { take } from "rxjs";

interface UserRef {
  id: number;
  name: string;
}

export interface Message {
  id: number;
  mission_id: number;
  sender: UserRef;
  type: 'log' | 'message';
  message: string;
  created_at: Date;
}

@Injectable()
export class MissionMessageService {

  constructor(private api: ApiService) { }

  getMissionMessages$(missionId: number) {
    return this.api.get$<Message[]>(`missions/${missionId}/messages`);
  }

  postMissionMessages$(missionId: number, message: string) {
    return this.api.post$<any, { message: string }>(`missions/${missionId}/messages`, { message })
        .pipe(take(1));
  }
}
