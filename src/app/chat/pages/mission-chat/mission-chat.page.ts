import { Component, DestroyRef, inject, Input, numberAttribute, OnInit } from '@angular/core';
import { FormsModule } from "@angular/forms";
import { Message, MissionMessageService } from "../../services/mission-message.service";
import { AsyncPipe, DatePipe, NgForOf, NgIf } from "@angular/common";
import { AuthService } from "../../../core/services/auth.service";
import { PusherService } from "../../../core/services/pusher.service";
import { Mission } from "../../../missions/services/missions.service";
import { BehaviorSubject, catchError, delay, of, take, tap } from "rxjs";

@Component({
    selector: 'app-mission-chat',
    templateUrl: './mission-chat.page.html',
    styleUrls: ['./mission-chat.page.scss'],
    standalone: true,
    imports: [
        AsyncPipe,
        FormsModule,
        NgForOf,
        DatePipe,
        NgIf
    ],
    providers: [
      MissionMessageService,
    ]
})
export class MissionChatPage implements OnInit {
    protected message = '';
    protected auth = inject(AuthService);
    private pusher = inject(PusherService);
    private missionMessage = inject(MissionMessageService);
    protected inputDisabled = false;

    @Input({ transform: numberAttribute }) id = 0;

    protected messages$ = new BehaviorSubject<Message[]>([]);

    constructor() {
      const destroyRef = inject(DestroyRef);
      destroyRef.onDestroy(() => this.stopListening());
    }

    ngOnInit() {
        this.initMessages();
    }

    submit() {
        if (!this.message) {
            return;
        }

        this.missionMessage.postMissionMessages$(this.id, this.message)
            .subscribe(() => this.message = '');
    }

    private initMessages() {
        this.missionMessage
            .getMissionMessages$(this.id)
            .pipe(
                tap(() => this.listenToSocket()),
                catchError(({ error }) => {
                    this.inputDisabled = true;
                    return of([{
                        id: 0,
                        type: 'log',
                        message: error.message,
                        sender: { id: 0, name: '' },
                        mission_id: this.id
                    }] as Message[]);
                }),
                tap(messages => this.messages$.next(messages)),
                delay(0),
                take(1)
            )
            .subscribe(this.scrollToBottom);
    }

    private listenToSocket() {
        this.pusher.listenOn(`missions.${this.id}`, `message`, (event: {
            mission: Mission,
            message: Message
        }) => {
            this.messages$.next([...this.messages$.getValue(), event.message]);
            this.scrollToBottom();
        });
    }

    private stopListening() {
      this.pusher.stopListening(`missions.${this.id}`, `message`);
    }

    private scrollToBottom() {
        window.scrollTo(0, document.body.scrollHeight);
    }
}
