import { ComponentFixture, TestBed } from '@angular/core/testing';

import { MissionChatPage } from './mission-chat.page';

describe('MissionChatComponent', () => {
  let component: MissionChatPage;
  let fixture: ComponentFixture<MissionChatPage>;

  beforeEach(() => {
    TestBed.configureTestingModule({
      declarations: [MissionChatPage]
    });
    fixture = TestBed.createComponent(MissionChatPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
