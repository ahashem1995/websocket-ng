import { CanActivateFn, Router } from '@angular/router';
import { inject } from "@angular/core";
import { AuthService } from "../services/auth.service";
import { catchError, map, of } from "rxjs";

export const authGuard: CanActivateFn = () => {
  const auth = inject(AuthService);
  const router = inject(Router);

  if (auth.isAuthenticated) {
    return (auth.user ? of(auth.user) : auth.me$())
        .pipe(
            map(() => true),
            catchError(() => of(false))
        );
  }

  return router.parseUrl('/login');
};
