import { Injectable } from '@angular/core';
import { ApiService } from "./api.service";
import { Observable, take, tap } from "rxjs";

interface User {
  id: number;
  name: string;
  email: string;
}

interface LoginResponse {
  user: User;
  token: string
}

@Injectable({ providedIn: 'root' })
export class AuthService {
  private tokenKey = 'token' as const;
  user: User | null = null;

  constructor(private api: ApiService) { }

  login$(email: string, password: string): Observable<any> {
    return this.api.post$<any, { email: string, password: string }>('login', { email, password })
      .pipe(
        tap((response: LoginResponse) => this.user = response.user),
        tap((response: LoginResponse) => this.setAuthToken(response.token))
      );
  }

  logout$(): Observable<any> {
    return this.api.post$<any, {}>('logout', {})
      .pipe(tap(() => this.removeAuthToken()))
  }

  me$(): Observable<User> {
    return this.api.get$<User>('user')
        .pipe(
            tap((user: User) => this.user = user),
            take(1)
        );
  }

  getAuthToken(): string | null {
    return localStorage.getItem(this.tokenKey);
  }

  setAuthToken(token: string) {
    return localStorage.setItem(this.tokenKey, token);
  }

  removeAuthToken() {
    return localStorage.removeItem(this.tokenKey);
  }

  get isAuthenticated() {
    return !!this.getAuthToken();
  }
}
