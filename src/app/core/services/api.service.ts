import { Injectable } from '@angular/core';
import { HttpClient } from "@angular/common/http";
import { Observable } from "rxjs";
import { environment } from "../../../environments/environment";

@Injectable({
  providedIn: 'root'
})
export class ApiService {
  private baseUrl = environment.API_BASE_URL;

  constructor(
    private http: HttpClient
  ) { }

  get$<T extends unknown>(url: string): Observable<T> {
    return this.http.get<T>(this.baseUrl + url);
  }

  post$<T extends unknown, E extends unknown>(url: string, body: E): Observable<T> {
    return this.http.post<T>(this.baseUrl + url, body);
  }
}
