import { inject, Injectable } from '@angular/core';
import Pusher from 'pusher-js';
import Echo from 'laravel-echo';
import { environment } from "../../../environments/environment";
import { HttpClient } from "@angular/common/http";

// @ts-ignore
window.Pusher = Pusher;


@Injectable({
  providedIn: 'root'
})
export class PusherService {
  private http = inject(HttpClient);

  private echo = new Echo({
    authorizer: (channel: { name: string }) => {
      return {
        authorize: (socketId: string, callback: (arg1: any, arg2?: any) => void) => {
          this.authenticate$({
            socket_id: socketId,
            channel_name: channel.name
          }).subscribe({
            next: (response: any) => callback(null, response),
            error: error => callback(error)
          })
        }
      };
    },
    broadcaster: 'pusher',
    key: environment.PUSHER_APP_KEY,
    cluster: 'mt1',
    wsHost: environment.PUSHER_HOST,
    wsPort: environment.PUSHER_PORT,
    wssPort: environment.PUSHER_PORT,
    forceTLS: false,
    encrypted: false,
    disableStats: true,
    enabledTransports: ['ws', 'wss'],
  });

  listenOn(channel: string, event: string, cb: (event: any) => void) {
    this.echo.private(channel).listen(`.${event}`, cb);
  }

  listenOnPublic(channel: string, event: string, cb: (event: any) => void) {
    this.echo.channel(channel).listen(`.${event}`, cb);
  }

  stopListening(channel: string, event: string) {
    this.echo.private(channel).stopListening(event);
  }

  private authenticate$(payload: { socket_id: string, channel_name: string }) {
    const url = `${environment.API_BASE_URL.split('/api/')[0]}/broadcasting/auth`;
    return this.http.post(url, payload);
  }


}
