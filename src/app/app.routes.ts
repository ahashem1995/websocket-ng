import { Routes } from "@angular/router";
import { authGuard } from "./core/guards/auth.guard";

export const routes: Routes = [
  {
    path: 'missions',
    loadComponent: () => import('./missions/pages/missions/missions.page').then(m => m.MissionsPage),
    canMatch: [authGuard]
  },
  {
    path: 'missions/:id/chat',
    loadComponent: () => import('./chat/pages/mission-chat/mission-chat.page').then(m => m.MissionChatPage),
    canMatch: [authGuard]
  },
  {
    path: 'login',
    loadComponent: () => import('./auth/pages/login/login.page').then(m => m.LoginPage),
  },
  {
    path: '',
    redirectTo: 'missions',
    pathMatch: 'full'
  }
];
