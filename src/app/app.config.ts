import { ApplicationConfig } from "@angular/core";
import { provideHttpClient, withInterceptors, } from "@angular/common/http";
import { authInterceptor } from "./core/interceptors/auth.interceptor";
import { provideRouter, withComponentInputBinding } from "@angular/router";
import { routes } from "./app.routes";

export const appConfig: ApplicationConfig = {
    providers: [
        provideHttpClient(withInterceptors([authInterceptor])),
        provideRouter(routes, withComponentInputBinding()),
    ]
}
