import { Component } from '@angular/core';
import { FormsModule } from "@angular/forms";
import { AuthService } from "../../../core/services/auth.service";
import { finalize, take } from "rxjs";
import { NgIf } from "@angular/common";
import { Router } from "@angular/router";

@Component({
  selector: 'app-login',
  templateUrl: './login.page.html',
  styleUrls: ['./login.page.scss'],
  standalone: true,
  imports: [
    FormsModule,
    NgIf
  ]
})
export class LoginPage {
  protected email = '';
  protected password = '';
  protected error = ''
  protected isLoading = false;

  constructor(
    private auth: AuthService,
    private router: Router,
  ) {
  }

  protected login() {
    this.error = '';
    this.isLoading = true;
    this.auth.login$(this.email, this.password)
      .pipe(
        take(1),
        finalize(() => this.isLoading = false),
      )
      .subscribe({
          next: () => {
            this.router.navigate(['/missions']).then()
          },
          error: error => this.error = error.error.message,
        }
      );
  }
}
